# Is There Phonological Feature Priming

This repository hosts the data that was analyzed and included in the paper entitled "Is There Phonological Feature Priming?".

This repository hosts the data that was analyzed and included in the paper entitled "Is There Phonological Feature Priming?".

The citation for the paper (will be updated once the paper is published):
Durvasula, K. & Parrish, A. (xxxx). Is There Phonological Feature Priming? *Linguistic Vanguard*.

The repository contains two csv files, corresponding to the data from the two experiments presented in the paper.